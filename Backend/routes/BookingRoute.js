const express = require('express');
const router = express.Router();
const seats = require('../models/seat');
const User = require('../models/user');
const passenger = require('../models/passenger');
const bookingDetail = require('../models/bookingDetail');

//To book a seat*
router.post('/bookSeat', async(req, res)=>{
    try{
        const seat = await seats.findById(req.body.seatId);
        if(seat){
            if(seat.seatStatus=="open"){
                let input = {};  
                input.name = req.body.name;
                input.mobile = req.body.mobile;
                input.age = req.body.age;
                input.gender = req.body.gender;
                input.source = req.body.source;
                input.destination = req.body.destination;
                input.travelDate = req.body.travelDate;
            
                let passengerModel = new passenger(input);
                passengerModel.save((err,result) =>{
                    if(err) {
                        res.send("Internal Server Error : Insert Passenger")
                    } else {
                        passenger.passengerId = passengerModel._id;
                        let bookTicket = {}
                        bookTicket.seatId = req.body.seatId;
                        bookTicket.passengerId = passenger.passengerId;
                        bookTicket.userId = req.body.userId;
            
                        let ticketDetail =  new bookingDetail(bookTicket);
                        ticketDetail.save((err,result)=> {
                            if(result) {
                                try{
                                    const seatdetails = {
                                        seatStatus : "close"
                                    }
                                    seats.findOneAndUpdate(
                                        {_id: req.body.seatId},
                                        {$set: seatdetails},
                                        {new: true}, (err, doc) => {
                                            if (err) {
                                                console.log("Something wrong when updating data!");
                                            }
                                        });
                                }
                                catch(err){
                                    console.log(err);
                                }                                     
                                res.json(bookTicket);
                            } else {
                                res.send("Internal Server Error - Booking Detail")
                            }
                        });
                    }
                });
            }
            else{
                res.send("Seat is already booked!!!");
            }
        }
        else{
            res.send("No such seat Available");
        }        
    }
    catch(err){
        res.json({message : err});
    }  
});

router.get('/passengerDetails', async (req,res)=>{
    try{
        const Alluser = await passenger.find();
        res.json(Alluser);
    }
    catch(err){
        res.json({message : err});
    }
});

//Cancel Booking
router.put('/cancelBooking', async (req, res)=>{
    try{
        const user = await User.findById(req.body.userId);
        if(user){
            await passenger.remove({_id : req.body.passengerId});
            await seats.findByIdAndUpdate({_id : req.body.seatId}, {$set : {seatStatus : "open"}}, {new : true});
            res.send("Booking Cancelled Successfully");
        }
        else{
            res.send("Only logged in users can cancel ticket.");
        }
    }
    catch(err){
        res.send(err);
    }
});

//View details of the person owning the ticket*
router.get('/bookingDetail/:seatId', async (req, res)=>{
    try{
        const seat = await seats.findById(req.params.seatId);
        //Get Passenger Detail for a Booked Seat
        if(seat){
            if(seat.seatStatus == "close"){
                let result = {};
                bookingDetail.find({seatId:req.params.seatId}, function(err, bookedDetail){
                    try{                        
                        if(!err && bookedDetail){
                            result = JSON.parse(JSON.stringify(bookedDetail))
                            passenger.find({_id:result[0].passengerId},function(error,passengerDetail){
                                if(!error && passengerDetail){
                                    res.json(passengerDetail)
                                }
                            });
                        }                        
                    }
                    catch(err){
                        res.send(err);
                    }
                });
            }
            else{
                res.send("You can book this seat!!");
            }
        }
        else{
            res.send("No such seats Available");
        }
    }
    catch(err){
        res.json({message : err});
    }      
})
module.exports = router;