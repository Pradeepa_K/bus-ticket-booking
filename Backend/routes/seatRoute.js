const express = require('express');
const router = express.Router();
const seats = require('../models/seat');
require('../models/user');
require('../models/passenger');

//view ticket status*
router.get('/seat/available', async (req,res)=>{
    try{
        const allSeat = await seats.find();
        let available=0, booked=0;
        allSeat.forEach(seat => {
            if(seat.seatStatus == "open"){
                available++;
            }
            else{
                booked++;
            }
        });
        let result = {
            Available: available,
            Booked: booked,
            Total: allSeat.length
        };
        res.json(result);
    }
    catch(err){
        res.json({message : err});
    }
});

// View all seats*
router.get('/seat/getSeats', async (req,res)=>{
    try{
        const s = await seats.find();
        res.json(s);
    }
    catch(err){
        res.json({message : err});
    }
});

//Add seat details*
router.post('/seat/insertSeat', (req, res) => {
    const{seatNo, seatStatus, seatDescription} = req.body;
    let seat = {};
    seat.seatNo = seatNo;
    seat.seatStatus = seatStatus;
    seat.seatDescription = seatDescription;
    let seatDetails = new seats(seat);
    seatDetails.save();
    res.json(seatDetails);
});

// View all open tickets*
router.get('/seat/open', async(req, res)=>{
    try{
        const isOpen = await seats.find({seatStatus : "open"});
        if(isOpen < 1)
            res.send("No open tickets, all Seats are Full.");
        else
            res.send(isOpen);
    }
    catch(err){
        res.send({message : err});
    }
});

// view all close tickets*
router.get('/seat/close', async(req, res)=>{
    try{
        const isClosed = await seats.find({seatStatus : "close"});
        if(isClosed < 1)
            res.send("No tickets to show.");
        else
            res.send(isClosed);
    }
    catch(err){
        res.send({message : err});
    }
});

//Get seat details by seatid*
router.get('/seat/:seatId', async (req, res)=>{
    try{
        const s = await seats.findById(req.params.seatId);
        if(s)
            res.send(s);
        else
            res.send("No such seat available");
    }
    catch(err){
        res.json({message : err});
    }
})

// Api for admin to reset the server*
router.post('/seat/reset', async(req, res)=>{    
    try{
        // const user = await User.findById(req.body.userId);
        // if(user.isAdmin){
            await seats.updateMany({}, {$set : {seatStatus : "open"}});
            res.send("Reset Successfull!!");
        // }
        // else{
            res.send("User cannot reset. Only admin can reset");
        // }    
    }
    catch(err){
        res.send(err);
    }
});

module.exports = router;