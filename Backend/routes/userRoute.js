const express = require('express');
const router = express.Router();
const User = require('../models/user');
const bcrypt = require('bcrypt');

//Add user details*
router.post('/user/insertUser', async(req, res)=>{
    try {
        const saltPassword = await bcrypt.genSalt(10);
        const securedPassword = await bcrypt.hash(req.body.password, saltPassword);
        
        let userDetail = {};
        userDetail.name = req.body.name,        
        userDetail.email = req.body.email,        
        userDetail.mobile = req.body.mobile,       
        userDetail.password = securedPassword, 
        userDetail.isAdmin = req.body.isAdmin
      
        let userObj = new User(userDetail);
        userObj.save();
        res.json(userObj);
    } catch(err){
        res.json({message : err});
    }  
});

//View all users*
router.get('/user/getUser', async (req,res)=>{
    try{
        const Alluser = await User.find();
        res.json(Alluser);
    }
    catch(err){
        res.json({message : err});
    }
});

//login
router.post('/login', async(req,res)=>{
    try{
        const user = await User.findOne({email : req.body.email, password : req.body.password});
        if(user)
            res.send(user);
        else
            res.send("Invalid email or password");
    }
    catch(err){
        res.json({message : err});
    }
});

//Get user by Id*
router.get('/user/:userId', async(req, res)=>{
    try{
        const user = await User.findById(req.params.userId);
        if(user)
            res.send(user);
        else
            res.send("No user found");
    }
    catch(err){
        res.json({message : err});
    }
});

//Edit user details*
router.put('/editUser/:userId', async(req, res)=>{
    try{
        const details = {
             passengerName  : req.body.passengerName,
             email  : req.body.email,
             mobileNumber  : req.body.mobileNumber,
             password  : req.body.password,
             isAdmin  : req.body.isAdmin
        }
        const upd = await User.findByIdAndUpdate(
            {_id: req.params.userId},
            {$set: details}, {new : true}
        );
        res.json(upd);
    }
    catch(err){
        res.json({message : err});
    }
});

module.exports = router;