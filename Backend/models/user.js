const mongoose = require('mongoose');

const userSchema = mongoose.Schema({
    name: {
        type: String,
        required: true,
    },
    email: {
        type: String,
        required: true,
        unique: true,
        lowercase: true,
    },
    mobile: {
        type: Number,
        required: true,
    },
    password: {
        type: String,
        required: true,
        minlength: 5,
    },
    createdOn: {
        type:Date,
        default: Date.now
    },
    isAdmin: {
        type: Boolean,
        default: false
    }
    
});

module.exports = mongoose.model('user', userSchema);