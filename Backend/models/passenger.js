const mongoose = require('mongoose');

const passengerSchema = mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    mobile: {
        type: Number,
        required: true
    },
    age: {
        type: Number,
        required: true
    },
    gender: {
        type: String,
        required: true
    },
    travelDate: {
        type:Date,
        required: true
    },
    source : {
        type : String,
        required : true
    },
    destination : {
        type : String,
        required : true
    }

});

module.exports = mongoose.model('passenger', passengerSchema);