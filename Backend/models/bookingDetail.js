const mongoose = require('mongoose');
const bookingDetailSchema = mongoose.Schema({

    seatId : {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'seat'
    }, 
    passengerId : {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'passenger'
    }, 
    userId : {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'user'
    },
    bookingDate : {
        type:Date,
        default: Date.now
    }
});
module.exports = mongoose.model('bookingDetail', bookingDetailSchema);