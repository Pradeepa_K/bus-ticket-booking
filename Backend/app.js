const express = require('express');
const mongoose = require('mongoose');
const app = express();
const bodyParser = require('body-parser');
require('./models/user');
require('./models/seat');
require('dotenv/config');
const cors = require('cors');

app.use(cors());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.json());

const userRoute = require('./routes/userRoute');
const seatRoute = require('./routes/seatRoute');
const bookingRoute = require('./routes/BookingRoute')

app.use(userRoute);
app.use(seatRoute);
app.use(bookingRoute);

app.get('/', (req, res)=> {
    res.send("Home Route");
})

mongoose.connect(process.env.MONGODB_URI,{
    useNewUrlParser: true ,
    useUnifiedTopology: true,
    useCreateIndex: true,
    useFindAndModify: false
});
mongoose.connection.on('connected',()=>{
    console.log('DB Connected');

})

const port = process.env.PORT || 4000;
app.listen(port, () => console.log(`Listening in http://localhost:${port}`));

